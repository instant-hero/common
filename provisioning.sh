#!/bin/bash

curl -fsSL https://get.docker.com -o get-docker.sh
chmod +x get-docker.sh
./get-docker.sh
systemctl enable docker
sed -i 's/StartLimitInterval=60s/StartLimitInterval=0s/g' /etc/systemd/system/multi-user.target.wants/docker.service
systemctl daemon-reload
service docker start
rm -f get-docker.sh

yum install -y epel-release
yum install -y nginx

yum install -y firewalld
systemctl enable firewalld
service firewalld restart
firewall-cmd --zone=public --permanent --add-port=80/tcp
firewall-cmd --zone=public --permanent --add-port=443/tcp
firewall-cmd --reload

cat > /etc/nginx/conf.d/instanthero.1oflads.com.conf <<EOF
server {
    listen 80;
    server_name instanthero.1oflads.com;

    root /usr/share/nginx/html/;
    index index.html;
}
EOF

systemctl enable nginx
service nginx restart
yum install -y certbot-nginx
certbot --nginx -d instanthero.1oflads.com --non-interactive --agree-tos -m ivan.yonkov@codexio.bg

cat > /etc/nginx/conf.d/instanthero.1oflads.com.conf <<EOF
server {
    listen 80;
    server_name instanthero.1oflads.com;
    return 301 https://instanthero.1oflads.com;
}

server {
    listen 443 ssl http2;
    server_name instanthero.1oflads.com;

    location / {
        proxy_pass http://localhost:4200;
    }

    location /api/ {
        rewrite ^api/(.*)$ $1 break;
        proxy_pass http://localhost:3000/;
    }

    location ~* \.io {
        proxy_pass http://localhost:3001;
        proxy_redirect off;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host \$host;
    }

    ssl_certificate /etc/letsencrypt/live/instanthero.1oflads.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/instanthero.1oflads.com/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}
EOF

touch renew-cron
echo "04 4 * * * /usr/bin/certbot renew --quiet" >> renew-cron
crontab renew-cron
rm -f renew-cron

service docker restart
docker network create reverse_proxy_network
docker network create broker_network
docker network create database_network
docker network create rtc_network

mkdir -p /root/db/data
mkdir -p /root/db/schema

cat > /root/db/schema/init.sql <<EOF
CREATE DATABASE IF NOT EXISTS \`instanthero\` DEFAULT CHARACTER SET utf8mb4;
CREATE TABLE IF NOT EXISTS \`disaster_type\` (
    \`id\` int(11) NOT NULL AUTO_INCREMENT,
    \`name\` varchar(255) NOT NULL,
    \`iconUrl\` varchar(255) NOT NULL,
    PRIMARY KEY (\`id\`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO \`disaster_type\` (id, name, iconUrl) VALUES (1, 'Пожар', 'fas fa-fire');
INSERT INTO \`disaster_type\` (id, name, iconUrl) VALUES (2, 'Наводнение', 'fas fa-water');
INSERT INTO \`disaster_type\` (id, name, iconUrl) VALUES (3, 'Инцидент', 'fas fa-car-crash');
INSERT INTO \`disaster_type\` (id, name, iconUrl) VALUES (4, 'Замърсяване', 'fas fa-smog');
EOF

cat > /root/.ssh/authorized_keys <<EOF

ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDqx/z9R70xvG8Wp8yry8zM7Qvtt5iU/AFHsI40Kol6p4EmCZAlBTo5ekQru1Zx/Lq16B71NirVyTpaHmDdhtK61D59+tE4pyCV6pK/BQbx4sBnwq8Xn+hJKUNNNeAmz/M9jBKalGP/bYNf9E8PPHDUCX7zgCoaa/BfgjBh8J43wITfuOCdLthIXQ5C27sIdQaRBTnFkNaSCo/rc1v/Ez9SN0Z85hi+qF3sNxiZPHxAlB85vsmDmVK4grT+gqYEUtxh6M7ldMEaWinmZZfgMPrzjIPCQm9GSBWTiLIDosyBCLIzQteXxGOiCIO+6pSUTbAgI7NjqD8aJjxGfjOejmYUibYYKgFBXItG6BMClQpIiy95s7172M287U1rRjP+psZ6btsS+av/z8BRbZk8MRxQyEy+HIjjvZVUZo5ySS+54HDdclu1y0fzKc01NMIXlJTp+N1YFswwbNnzm5ZaDlnc25RyTIeqcuZS9s1x0Aw8QIQ5+Fi8vnCUDifUHmT4H+U= ivany@DESKTOP-MDEOCC2
EOF

docker login docker.codexio.bg -u codexio-docker -p "${DOCKER_REGISTRY_PASS}"

setsebool -P httpd_can_network_connect 1
service nginx restart


