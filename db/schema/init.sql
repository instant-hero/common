CREATE DATABASE IF NOT EXISTS `disaster-volunteers` DEFAULT CHARACTER SET utf8mb4;
CREATE TABLE IF NOT EXISTS `disaster_type` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `iconUrl` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `disaster_type` (id, name, iconUrl) VALUES (1, 'Пожар', 'fas fa-fire');
INSERT INTO `disaster_type` (id, name, iconUrl) VALUES (2, 'Наводнение', 'fas fa-water');
INSERT INTO `disaster_type` (id, name, iconUrl) VALUES (3, 'Инцидент', 'fas fa-car-crash');
INSERT INTO `disaster_type` (id, name, iconUrl) VALUES (4, 'Замърсяване', 'fas fa-smog');
